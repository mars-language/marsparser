// Generated from /home/lab/mars/parser/marsparser/MarsParser.g4 by ANTLR 4.13.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MarsParserParser}.
 */
public interface MarsParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code MarsFile}
	 * labeled alternative in {@link MarsParserParser#marsfile}.
	 * @param ctx the parse tree
	 */
	void enterMarsFile(MarsParserParser.MarsFileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsFile}
	 * labeled alternative in {@link MarsParserParser#marsfile}.
	 * @param ctx the parse tree
	 */
	void exitMarsFile(MarsParserParser.MarsFileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsNode}
	 * labeled alternative in {@link MarsParserParser#node}.
	 * @param ctx the parse tree
	 */
	void enterMarsNode(MarsParserParser.MarsNodeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsNode}
	 * labeled alternative in {@link MarsParserParser#node}.
	 * @param ctx the parse tree
	 */
	void exitMarsNode(MarsParserParser.MarsNodeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsPreamble}
	 * labeled alternative in {@link MarsParserParser#preamble}.
	 * @param ctx the parse tree
	 */
	void enterMarsPreamble(MarsParserParser.MarsPreambleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsPreamble}
	 * labeled alternative in {@link MarsParserParser#preamble}.
	 * @param ctx the parse tree
	 */
	void exitMarsPreamble(MarsParserParser.MarsPreambleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsNodeSpecUses}
	 * labeled alternative in {@link MarsParserParser#use}.
	 * @param ctx the parse tree
	 */
	void enterMarsNodeSpecUses(MarsParserParser.MarsNodeSpecUsesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsNodeSpecUses}
	 * labeled alternative in {@link MarsParserParser#use}.
	 * @param ctx the parse tree
	 */
	void exitMarsNodeSpecUses(MarsParserParser.MarsNodeSpecUsesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsConstDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void enterMarsConstDeclaration(MarsParserParser.MarsConstDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsConstDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void exitMarsConstDeclaration(MarsParserParser.MarsConstDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsVariableDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void enterMarsVariableDeclaration(MarsParserParser.MarsVariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsVariableDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void exitMarsVariableDeclaration(MarsParserParser.MarsVariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsExternDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void enterMarsExternDeclaration(MarsParserParser.MarsExternDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsExternDeclaration}
	 * labeled alternative in {@link MarsParserParser#declarations}.
	 * @param ctx the parse tree
	 */
	void exitMarsExternDeclaration(MarsParserParser.MarsExternDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsDataTypes}
	 * labeled alternative in {@link MarsParserParser#dt}.
	 * @param ctx the parse tree
	 */
	void enterMarsDataTypes(MarsParserParser.MarsDataTypesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsDataTypes}
	 * labeled alternative in {@link MarsParserParser#dt}.
	 * @param ctx the parse tree
	 */
	void exitMarsDataTypes(MarsParserParser.MarsDataTypesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsIdAndDatatype}
	 * labeled alternative in {@link MarsParserParser#iddt}.
	 * @param ctx the parse tree
	 */
	void enterMarsIdAndDatatype(MarsParserParser.MarsIdAndDatatypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsIdAndDatatype}
	 * labeled alternative in {@link MarsParserParser#iddt}.
	 * @param ctx the parse tree
	 */
	void exitMarsIdAndDatatype(MarsParserParser.MarsIdAndDatatypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsNodeBody}
	 * labeled alternative in {@link MarsParserParser#body}.
	 * @param ctx the parse tree
	 */
	void enterMarsNodeBody(MarsParserParser.MarsNodeBodyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsNodeBody}
	 * labeled alternative in {@link MarsParserParser#body}.
	 * @param ctx the parse tree
	 */
	void exitMarsNodeBody(MarsParserParser.MarsNodeBodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsChoreo}
	 * labeled alternative in {@link MarsParserParser#choreo}.
	 * @param ctx the parse tree
	 */
	void enterMarsChoreo(MarsParserParser.MarsChoreoContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsChoreo}
	 * labeled alternative in {@link MarsParserParser#choreo}.
	 * @param ctx the parse tree
	 */
	void exitMarsChoreo(MarsParserParser.MarsChoreoContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsIfOrchestration}
	 * labeled alternative in {@link MarsParserParser#mroschoreo}.
	 * @param ctx the parse tree
	 */
	void enterMarsIfOrchestration(MarsParserParser.MarsIfOrchestrationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsIfOrchestration}
	 * labeled alternative in {@link MarsParserParser#mroschoreo}.
	 * @param ctx the parse tree
	 */
	void exitMarsIfOrchestration(MarsParserParser.MarsIfOrchestrationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSpinOrchestration}
	 * labeled alternative in {@link MarsParserParser#orchestration}.
	 * @param ctx the parse tree
	 */
	void enterMarsSpinOrchestration(MarsParserParser.MarsSpinOrchestrationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSpinOrchestration}
	 * labeled alternative in {@link MarsParserParser#orchestration}.
	 * @param ctx the parse tree
	 */
	void exitMarsSpinOrchestration(MarsParserParser.MarsSpinOrchestrationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsRtosOrchestration}
	 * labeled alternative in {@link MarsParserParser#orchestration}.
	 * @param ctx the parse tree
	 */
	void enterMarsRtosOrchestration(MarsParserParser.MarsRtosOrchestrationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsRtosOrchestration}
	 * labeled alternative in {@link MarsParserParser#orchestration}.
	 * @param ctx the parse tree
	 */
	void exitMarsRtosOrchestration(MarsParserParser.MarsRtosOrchestrationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MarsParserParser#unit}.
	 * @param ctx the parse tree
	 */
	void enterUnit(MarsParserParser.UnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link MarsParserParser#unit}.
	 * @param ctx the parse tree
	 */
	void exitUnit(MarsParserParser.UnitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardTag}
	 * labeled alternative in {@link MarsParserParser#hardtag}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardTag(MarsParserParser.MarsHardTagContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardTag}
	 * labeled alternative in {@link MarsParserParser#hardtag}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardTag(MarsParserParser.MarsHardTagContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSoftTag}
	 * labeled alternative in {@link MarsParserParser#softtag}.
	 * @param ctx the parse tree
	 */
	void enterMarsSoftTag(MarsParserParser.MarsSoftTagContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSoftTag}
	 * labeled alternative in {@link MarsParserParser#softtag}.
	 * @param ctx the parse tree
	 */
	void exitMarsSoftTag(MarsParserParser.MarsSoftTagContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSpinTag}
	 * labeled alternative in {@link MarsParserParser#spintag}.
	 * @param ctx the parse tree
	 */
	void enterMarsSpinTag(MarsParserParser.MarsSpinTagContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSpinTag}
	 * labeled alternative in {@link MarsParserParser#spintag}.
	 * @param ctx the parse tree
	 */
	void exitMarsSpinTag(MarsParserParser.MarsSpinTagContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardTrigger}
	 * labeled alternative in {@link MarsParserParser#softtrigg}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardTrigger(MarsParserParser.MarsHardTriggerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardTrigger}
	 * labeled alternative in {@link MarsParserParser#softtrigg}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardTrigger(MarsParserParser.MarsHardTriggerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSoftTrigger}
	 * labeled alternative in {@link MarsParserParser#hardtrigg}.
	 * @param ctx the parse tree
	 */
	void enterMarsSoftTrigger(MarsParserParser.MarsSoftTriggerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSoftTrigger}
	 * labeled alternative in {@link MarsParserParser#hardtrigg}.
	 * @param ctx the parse tree
	 */
	void exitMarsSoftTrigger(MarsParserParser.MarsSoftTriggerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsFretReq}
	 * labeled alternative in {@link MarsParserParser#requirement}.
	 * @param ctx the parse tree
	 */
	void enterMarsFretReq(MarsParserParser.MarsFretReqContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsFretReq}
	 * labeled alternative in {@link MarsParserParser#requirement}.
	 * @param ctx the parse tree
	 */
	void exitMarsFretReq(MarsParserParser.MarsFretReqContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardSporadicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardSporadicTask(MarsParserParser.MarsHardSporadicTaskContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardSporadicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardSporadicTask(MarsParserParser.MarsHardSporadicTaskContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardPeriodicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardPeriodicTask(MarsParserParser.MarsHardPeriodicTaskContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardPeriodicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardPeriodicTask(MarsParserParser.MarsHardPeriodicTaskContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSoftSporadicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void enterMarsSoftSporadicTask(MarsParserParser.MarsSoftSporadicTaskContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSoftSporadicTask}
	 * labeled alternative in {@link MarsParserParser#task}.
	 * @param ctx the parse tree
	 */
	void exitMarsSoftSporadicTask(MarsParserParser.MarsSoftSporadicTaskContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardPeriodicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardPeriodicMonitor(MarsParserParser.MarsHardPeriodicMonitorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardPeriodicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardPeriodicMonitor(MarsParserParser.MarsHardPeriodicMonitorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSoftPeriodicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void enterMarsSoftPeriodicMonitor(MarsParserParser.MarsSoftPeriodicMonitorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSoftPeriodicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void exitMarsSoftPeriodicMonitor(MarsParserParser.MarsSoftPeriodicMonitorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsHardSporadicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void enterMarsHardSporadicMonitor(MarsParserParser.MarsHardSporadicMonitorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsHardSporadicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void exitMarsHardSporadicMonitor(MarsParserParser.MarsHardSporadicMonitorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsSoftSporadicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void enterMarsSoftSporadicMonitor(MarsParserParser.MarsSoftSporadicMonitorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsSoftSporadicMonitor}
	 * labeled alternative in {@link MarsParserParser#monitor}.
	 * @param ctx the parse tree
	 */
	void exitMarsSoftSporadicMonitor(MarsParserParser.MarsSoftSporadicMonitorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsFuncParams}
	 * labeled alternative in {@link MarsParserParser#funcparams}.
	 * @param ctx the parse tree
	 */
	void enterMarsFuncParams(MarsParserParser.MarsFuncParamsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsFuncParams}
	 * labeled alternative in {@link MarsParserParser#funcparams}.
	 * @param ctx the parse tree
	 */
	void exitMarsFuncParams(MarsParserParser.MarsFuncParamsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsPublish}
	 * labeled alternative in {@link MarsParserParser#publish}.
	 * @param ctx the parse tree
	 */
	void enterMarsPublish(MarsParserParser.MarsPublishContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsPublish}
	 * labeled alternative in {@link MarsParserParser#publish}.
	 * @param ctx the parse tree
	 */
	void exitMarsPublish(MarsParserParser.MarsPublishContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsProgAssg}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterMarsProgAssg(MarsParserParser.MarsProgAssgContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsProgAssg}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitMarsProgAssg(MarsParserParser.MarsProgAssgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsProgPublish}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterMarsProgPublish(MarsParserParser.MarsProgPublishContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsProgPublish}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitMarsProgPublish(MarsParserParser.MarsProgPublishContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsProgFunctionCall}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterMarsProgFunctionCall(MarsParserParser.MarsProgFunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsProgFunctionCall}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitMarsProgFunctionCall(MarsParserParser.MarsProgFunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsProgIf}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterMarsProgIf(MarsParserParser.MarsProgIfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsProgIf}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitMarsProgIf(MarsParserParser.MarsProgIfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MarsProgWhile}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterMarsProgWhile(MarsParserParser.MarsProgWhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MarsProgWhile}
	 * labeled alternative in {@link MarsParserParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitMarsProgWhile(MarsParserParser.MarsProgWhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprIsAExpr}
	 * labeled alternative in {@link MarsParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExprIsAExpr(MarsParserParser.ExprIsAExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprIsAExpr}
	 * labeled alternative in {@link MarsParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExprIsAExpr(MarsParserParser.ExprIsAExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ExprIsBExpr}
	 * labeled alternative in {@link MarsParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExprIsBExpr(MarsParserParser.ExprIsBExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ExprIsBExpr}
	 * labeled alternative in {@link MarsParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExprIsBExpr(MarsParserParser.ExprIsBExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprMulOrDiv}
	 * labeled alternative in {@link MarsParserParser#aexpr}.
	 * @param ctx the parse tree
	 */
	void enterAExprMulOrDiv(MarsParserParser.AExprMulOrDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprMulOrDiv}
	 * labeled alternative in {@link MarsParserParser#aexpr}.
	 * @param ctx the parse tree
	 */
	void exitAExprMulOrDiv(MarsParserParser.AExprMulOrDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprATerm}
	 * labeled alternative in {@link MarsParserParser#aexpr}.
	 * @param ctx the parse tree
	 */
	void enterAExprATerm(MarsParserParser.AExprATermContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprATerm}
	 * labeled alternative in {@link MarsParserParser#aexpr}.
	 * @param ctx the parse tree
	 */
	void exitAExprATerm(MarsParserParser.AExprATermContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprSumOrSub}
	 * labeled alternative in {@link MarsParserParser#aterm}.
	 * @param ctx the parse tree
	 */
	void enterAExprSumOrSub(MarsParserParser.AExprSumOrSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprSumOrSub}
	 * labeled alternative in {@link MarsParserParser#aterm}.
	 * @param ctx the parse tree
	 */
	void exitAExprSumOrSub(MarsParserParser.AExprSumOrSubContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprATermU}
	 * labeled alternative in {@link MarsParserParser#aterm}.
	 * @param ctx the parse tree
	 */
	void enterAExprATermU(MarsParserParser.AExprATermUContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprATermU}
	 * labeled alternative in {@link MarsParserParser#aterm}.
	 * @param ctx the parse tree
	 */
	void exitAExprATermU(MarsParserParser.AExprATermUContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprNeg}
	 * labeled alternative in {@link MarsParserParser#atermu}.
	 * @param ctx the parse tree
	 */
	void enterAExprNeg(MarsParserParser.AExprNegContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprNeg}
	 * labeled alternative in {@link MarsParserParser#atermu}.
	 * @param ctx the parse tree
	 */
	void exitAExprNeg(MarsParserParser.AExprNegContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprAatom}
	 * labeled alternative in {@link MarsParserParser#atermu}.
	 * @param ctx the parse tree
	 */
	void enterAExprAatom(MarsParserParser.AExprAatomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprAatom}
	 * labeled alternative in {@link MarsParserParser#atermu}.
	 * @param ctx the parse tree
	 */
	void exitAExprAatom(MarsParserParser.AExprAatomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprInner}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void enterAExprInner(MarsParserParser.AExprInnerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprInner}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void exitAExprInner(MarsParserParser.AExprInnerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprInt}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void enterAExprInt(MarsParserParser.AExprIntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprInt}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void exitAExprInt(MarsParserParser.AExprIntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AExprID}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void enterAExprID(MarsParserParser.AExprIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AExprID}
	 * labeled alternative in {@link MarsParserParser#aatom}.
	 * @param ctx the parse tree
	 */
	void exitAExprID(MarsParserParser.AExprIDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprAndOr}
	 * labeled alternative in {@link MarsParserParser#bexpr}.
	 * @param ctx the parse tree
	 */
	void enterBExprAndOr(MarsParserParser.BExprAndOrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprAndOr}
	 * labeled alternative in {@link MarsParserParser#bexpr}.
	 * @param ctx the parse tree
	 */
	void exitBExprAndOr(MarsParserParser.BExprAndOrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprBTerm}
	 * labeled alternative in {@link MarsParserParser#bexpr}.
	 * @param ctx the parse tree
	 */
	void enterBExprBTerm(MarsParserParser.BExprBTermContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprBTerm}
	 * labeled alternative in {@link MarsParserParser#bexpr}.
	 * @param ctx the parse tree
	 */
	void exitBExprBTerm(MarsParserParser.BExprBTermContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprAExpr}
	 * labeled alternative in {@link MarsParserParser#bterm}.
	 * @param ctx the parse tree
	 */
	void enterBExprAExpr(MarsParserParser.BExprAExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprAExpr}
	 * labeled alternative in {@link MarsParserParser#bterm}.
	 * @param ctx the parse tree
	 */
	void exitBExprAExpr(MarsParserParser.BExprAExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprBTermU}
	 * labeled alternative in {@link MarsParserParser#bterm}.
	 * @param ctx the parse tree
	 */
	void enterBExprBTermU(MarsParserParser.BExprBTermUContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprBTermU}
	 * labeled alternative in {@link MarsParserParser#bterm}.
	 * @param ctx the parse tree
	 */
	void exitBExprBTermU(MarsParserParser.BExprBTermUContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprNeg}
	 * labeled alternative in {@link MarsParserParser#btermu}.
	 * @param ctx the parse tree
	 */
	void enterBExprNeg(MarsParserParser.BExprNegContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprNeg}
	 * labeled alternative in {@link MarsParserParser#btermu}.
	 * @param ctx the parse tree
	 */
	void exitBExprNeg(MarsParserParser.BExprNegContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprBAtom}
	 * labeled alternative in {@link MarsParserParser#btermu}.
	 * @param ctx the parse tree
	 */
	void enterBExprBAtom(MarsParserParser.BExprBAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprBAtom}
	 * labeled alternative in {@link MarsParserParser#btermu}.
	 * @param ctx the parse tree
	 */
	void exitBExprBAtom(MarsParserParser.BExprBAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprInner}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void enterBExprInner(MarsParserParser.BExprInnerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprInner}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void exitBExprInner(MarsParserParser.BExprInnerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprVal}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void enterBExprVal(MarsParserParser.BExprValContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprVal}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void exitBExprVal(MarsParserParser.BExprValContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BExprID}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void enterBExprID(MarsParserParser.BExprIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BExprID}
	 * labeled alternative in {@link MarsParserParser#batom}.
	 * @param ctx the parse tree
	 */
	void exitBExprID(MarsParserParser.BExprIDContext ctx);
}