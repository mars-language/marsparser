JAVA=/usr/bin/java
OUTPUT=mars
GRAMMAR=MarsParser.g4
TOKENS=MarsLexer.g4

all: run

run: $(TOKENS) $(GRAMMAR)
	$(JAVA) -Xmx500M -cp /usr/local/lib/antlr-4.9.2-complete.jar org.antlr.v4.Tool -Dlanguage=Python3 \
	*.g4 -no-listener -visitor -o mars
	touch mars/__init__.py

clean:
	rm mars/*.py mars/*.tokens mars/*.interp