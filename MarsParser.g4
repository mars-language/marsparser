grammar MarsParser;

marsfile: nodedec preamble body #MarsFile;

/* node id and target platform */
nodedec: NODE node_id=ID TARGETS target_platform = STRING SEMICOLON #MarsNode;

/* imports and variable and const declaration */
preamble: (use | declarations)* #MarsPreamble;

/* use cardiowheel.h; */
use: USE file_id = FILENAME SEMICOLON #MarsNodeSpecUses;

/*  const BUFFER_SIZE 950; | let start_flag: bool = false; | extern max_ecg: i32, min_ecg: i32, newbatch: bool; */
declarations: CONST const_id = ID (COMMA constid = ID)* (WHOLE_N | FLOAT_N) SEMICOLON #MarsConstDeclaration
    | LET vard_id = ID (COMMA name = ID)* COLON data_type = dt (ASSIGNMENT ex=expr)? SEMICOLON #MarsVariableDeclaration
    | EXTERN iddt (COMMA iddt)* SEMICOLON #MarsExternDeclaration
    ;

/* data types */
dt: (U8 | I8 | I16 | U16 | I32 | U32 | F32 | STR | BOOL) #MarsDataTypes;

/* id and respective data type */
iddt: (extern_id = ID COLON datatype = dt) #MarsIdAndDatatype;

/* multiple task, monitor, and orchestration */
body: (task|monitor|orchestration)+ #MarsNodeBody;

/* specify task/monitor ordering */
choreo: ID (FOLLOWED_BY ID)* SEMICOLON #MarsChoreo;

/* code allowed inside spin */
mroschoreo: IF expr OPEN_BRACE choreo CLOSE_BRACE (ELSE IF expr OPEN_BRACE choreo CLOSE_BRACE)* (ELSE OPEN_BRACE choreo CLOSE_BRACE)? SEMICOLON #MarsIfOrchestration;

/* cores will either run  */
orchestration: SPIN COREID OPEN_BRACE mroschoreo CLOSE_BRACE SEMICOLON #MarsSpinOrchestration
    | RUN COREID OPEN_BRACE choreo+ CLOSE_BRACE SEMICOLON #MarsRtosOrchestration;

/* supported units of time  */
unit: SECONDS | MILLISEC | MICROSEC | NANOSEC #MarsTimeUnit;

/* #[09,2000,10ms,2ms,8ms] */
hardtag: OPEN_SCHED PRIO? v_prio = WHOLE_N COMMA
         STACK? v_stack = WHOLE_N COMMA
         ((PERIOD? v_period = (FLOAT_N | WHOLE_N) unit) | UNDERSCORE) COMMA
         WCET? v_wcet = (FLOAT_N | WHOLE_N) unit COMMA
         ((DEADLINE? v_deadline = (FLOAT_N | WHOLE_N) unit) | UNDERSCORE) CLOSE_BRACKET #MarsHardTag;

/* #[200ms] */
softtag: OPEN_SCHED (PERIOD? v_period = (FLOAT_N | WHOLE_N) unit) CLOSE_BRACKET #MarsSoftTag;

/* tbd */
spintag: OPEN_SCHED (((PERIOD)? v_wcet = WHOLE_N unit)) (((TIMEOUT)? v_deadline = WHOLE_N unit) | UNDERSCORE) CLOSE_BRACKET #MarsSpinTag;

/* <~| "/topicA" */
softtrigg: TRIGGERED_BY TOPIC (MATCHES ID)+ #MarsHardTrigger;

/* <~| funcA */
hardtrigg: TRIGGERED_BY ID #MarsSoftTrigger;

/* "m_ecg shall immeditately satisfy max_ecg < 3048 & min_ecg > 2000" */
requirement: VERIFIES STRING #MarsFretReq;

/* 4 types of tasks: hard periodic, hard sporadic, soft periodic, soft sporadic */
task: hardtag SPORADIC TASK task_id = ID hardtrigg OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsHardSporadicTask
    | hardtag PERIODIC TASK task_id = ID OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsHardPeriodicTask
    | softtag PERIODIC TASK task_id = ID OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsHardPeriodicTask
    | SPORADIC TASK task_id = ID softtrigg OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsSoftSporadicTask
    ;

/* 4 types of monitors: hard periodic, hard sporadic, soft periodic, soft sporadic */
monitor: hardtag PERIODIC MONITOR mon_id = ID requirement? ENFORCES OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsHardPeriodicMonitor
    | softtag PERIODIC MONITOR mon_id = ID requirement? ENFORCES OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsSoftPeriodicMonitor
    | hardtag SPORADIC MONITOR name=ID hardtrigg requirement? ENFORCES OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON  #MarsHardSporadicMonitor
    | SPORADIC MONITOR name=ID softtrigg requirement? ENFORCES OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON  #MarsSoftSporadicMonitor
    ;

/* double check */
funcparams: (ID (COMMA ID)*)? #MarsFuncParams;

pubparams: expr #MarsPubParams;

/* publish */
publish: PUB pubparams IN TOPIC SEMICOLON #MarsPublish;

ifrule : IF expr OPEN_BRACE prog+ CLOSE_BRACE
         (ELSE IF expr OPEN_BRACE prog+ CLOSE_BRACE)*
         (ELSE OPEN_BRACE prog+ CLOSE_BRACE)? SEMICOLON #MarsGenericIf ;

whilerule: WHILE expr OPEN_BRACE prog+ CLOSE_BRACE SEMICOLON #MarsWhile;

prog: var=ID ASSIGNMENT progexpr = expr SEMICOLON #MarsProgAssg
    | publish #MarsProgPublish
    | ID OPEN_PARENS funcparams CLOSE_PARENS SEMICOLON #MarsProgFunctionCall
    | ifrule #MarsProgIf
    | whilerule #MarsProgWhile
    ;

expr: ae = aexpr #ExprIsAExpr
    | be = bexpr #ExprIsBExpr;

aexpr: left = aexpr op = (STAR|DIV) right = aterm #AExprMulOrDiv
    | inner = aterm #AExprATerm;

aterm: left=aterm op=(PLUS|MINUS) right=atermu #AExprSumOrSub
    | inner=atermu #AExprATermU
    ;

atermu: MINUS inner=atermu  #AExprNeg
    |  inner=aatom   #AExprAatom
    ;

aatom: OPEN_PARENS inner = aexpr CLOSE_PARENS #AExprInner
    | num = WHOLE_N #AExprInt
    | var = ID #AExprID
    ;

bexpr: left = bexpr  op = (OP_AND|OP_OR) right = bterm #BExprAndOr
    | inner = bterm #BExprBTerm
    ;

bterm: left=aexpr  op=(OP_GT|OP_GE|OP_EQ|OP_NE|OP_LT|OP_LE) right=aexpr     #BExprAExpr
    | inner=btermu  #BExprBTermU
    ;

btermu: BANG inner=btermu   #BExprNeg
    | inner=batom   #BExprBAtom
    ;

batom: OPEN_PARENS inner=bexpr CLOSE_PARENS     #BExprInner
    | atom=(TRUE | FALSE)   #BExprVal
    | var=ID    #BExprID
    ;


/* boolean constants */
TRUE            : 'true';
FALSE           : 'false';

/* keywords for specification/declaration */
NODE            : 'node';
USE             : 'use';
LET             : 'let';
EXTERN          : 'extern';
CONST           : 'const';
TARGETS         : 'targets';

GROUP           : 'group';
TOPICS          : 'topics';

TASK            : 'task';
MONITOR         : 'monitor';
PERIODIC        : 'periodic';
SPORADIC        : 'sporadic';

TRIGGERED       : 'triggered';
BY              : 'by';

/* data types for numbers */
U8              : 'u8';
I8              : 'i8';
I16             : 'i16';
U16             : 'u16';
I32             : 'i32';
U32             : 'u32';
F32             : 'f32';
BOOL            : 'bool';
STR             : 'str';

/* scheduling tag */
PRIO            : 'P';
STACK           : 'STACK';
PERIOD          : 'T';
WCET            : 'C';
DEADLINE        : 'D';
TIMEOUT         : 'TIMEOUT';
MILLISEC        : 'ms';
MICROSEC        : 'us';
NANOSEC         : 'ns';
SECONDS         : 'sec';

/* keywords for actions */
PUB             : 'pub';
IN              : 'in';
ENFORCES        : 'enforces';
VERIFIES        : 'verifies';
RUN             : 'run';

/* ???? */
SPIN             : 'spin';
LAUNCH           : 'launch';

/* programming language fragment tokens */
IF              : 'if';
ELSE            : 'else';
WHILE           : 'while';

OPEN_SCHED      : '#[';
FOLLOWED_BY     : '~>';
TRIGGERED_BY    : 'triggered by';
TRIGGERS        : 'triggers';
MATCHES         : 'as';

/* operators and punctuators */
OPEN_BRACE      : '{';
CLOSE_BRACE     : '}';
OPEN_BRACKET    : '[';
CLOSE_BRACKET   : ']';
OPEN_PARENS     : '(';
CLOSE_PARENS    : ')';
DOT             : '.';
COMMA           : ',';
COLON           : ':';
SEMICOLON       : ';';
PLUS            : '+';
MINUS           : '-';
STAR            : '*';
DIV             : '/';
BANG            : '!';
ASSIGNMENT      : '=';
OP_LT           : '<';
OP_GT           : '>';
OP_AND          : '&';
OP_OR           : '|';
OP_EQ           : '==';
OP_NE           : '!=';
OP_LE           : '<=';
OP_GE           : '>=';
UNDERSCORE      : '_';

ID              : LETTER (LETTER | UNDERSCORE | [0-9])* ;
COREID          : UNDERSCORE (LETTER | [0-9])* ;
FILENAME        : LETTER (LETTER | [0-9])*'.'LETTER (LETTER | [0-9])*;
/* OLD STRING   : '"/' (~["\r\n] | '""')* '"'; */
TOPIC           : '"/' (~["\r\n])* '"';
STRING          : '"'(~["\r\n])+'"';

fragment LETTER : [a-zA-Z] ;
fragment INT    : '0' | '1'..'9' '0'..'9'* ;
WHOLE_N         : INT;
FLOAT_N         : (INT '.' INT*) | ('.' INT+);
WS              : [ \r\n]+ -> skip ;
